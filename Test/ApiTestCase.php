<?php

namespace Nerds\BaseBundle\Test;

use GuzzleHttp\Client;
use Liip\FunctionalTestBundle\Test\WebTestCase as WebTestCase;

class ApiTestCase extends WebTestCase
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected static $client;

    /**
     * @var array
     */
    protected static $data;

    public static function setUpBeforeClass()
    {
        self::$client = new Client();
        self::$data = [
            'email'     => 'ss@a2a.co',
            'password'  => '123456',
            'first'     => 'Sergey',
            'last'      => 'Slepokurov',
            'birthDate' => '1986-09-20'
        ];
    }

    /**
     * @param \GuzzleHttp\Message\ResponseInterface $response
     */
    protected function assertJsonResponse($response)
    {
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json', $response->getHeader('Content-type'));
    }

    protected function output($data)
    {
        fwrite(STDOUT, $data);
    }
}