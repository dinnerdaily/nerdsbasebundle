<?php

namespace Nerds\BaseBundle\Controller;

use Nerds\BaseBundle\Util\APIException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nerds\BaseBundle\Util\APIError;

class PhoneController extends Controller
{
    protected $reflectionPath;

    public function callAction(Request $request, $class, $name, $namespace = false)
    {
        if(!$this->reflectionPath) {
            $this->reflectionPath = $this->container->getParameter('nerds.api.namespace');
        }

        $param = array();
        $data = array();
        $error = null;

        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $param = json_decode($request->getContent(), true);
            if(count($request->query->all()) > 0){
                foreach($request->query->all() as $key => $value){
                    $param[$key] = $value;
                }
            }
        } elseif ($request->getMethod() == 'GET') {
            $param = $request->query->all();
        } else {
            $param = $request->request->all();
            if(count($request->query->all()) > 0){
                foreach($request->query->all() as $key => $value){
                    $param[$key] = $value;
                }
            }
        }
        if ($this->container->has('nerds.analytics')) {
            $this->container->get('nerds.analytics')->setAnalytics($request);
        }

        try {
            if($namespace) {
                $class = $this->reflectionPath . '\\' . ucfirst($namespace) . '\\' . ucfirst($class);
            } else {
                $class = $this->reflectionPath . '\\' . ucfirst($class);
            }

            $context = new \ReflectionClass($class);
            $obj = $context->newInstance();
            $setContext = new \ReflectionMethod($class, 'setContext');
            $method = new \ReflectionMethod($class, $name);

            $setContext->invoke($obj, $this, $this->container, $request);

            $data = $method->invoke($obj, $param);
            if($this->get('kernel')->getEnvironment() == 'dev') {
                $logger = $this->get('logger');
                $logger->debug(
                    $name,
                    array(
                        'request' => $param,
                        'response' => $data,
                        'url' => $class
                    ));
            }
        }
        catch(\ReflectionException $ex) {
            $logger = $this->get('logger');
            $logger->error($ex->getMessage(), array('request' => $param, 'url' => $class, 'method' => $name));

            if($this->get('kernel')->getEnvironment() == 'dev') {
                $error = new APIError(APIError::WRONG_API_PATH, $ex->getMessage());
            } else {
                $error = new APIError(APIError::WRONG_API_PATH, "Wrong api path, please check for valid url");
            }
        }
        catch(\Exception $ex) {
            $error =  new APIError($ex->getCode(), $ex->getMessage());
            $previous = $ex->getPrevious();
            $code = uniqid();
            if ($previous) {
                $error->setSubCode($previous->getCode());
                $error->setSubMessage($previous->getMessage());
            }
            $logger = $this->get('logger');

            $param["auth-token"] = $request->headers->get('token');

            if($this->container->has('monolog.logger.nerds.trace')) {
                $logger->error(
                    $ex->getMessage(),
                    array(
                        'request' => $param,
                        'url' => $class,
                        'method' => $name,
                        'error' => $error ? $error->toJson() : $error,
                        'uid' => $code,
                        'exceptionType' => get_class($ex)
                    )
                );

                $this->container->get('monolog.logger.nerds.trace')->error(
                    $ex->getMessage(),
                    array(
                        'request' => $param,
                        'url' => $class,
                        'method' => $name,
                        'error' => $error ? $error->toJson() : $error,
                        'uid' => $code,
                        'exceptionType' => get_class($ex),
                        'trace' => $ex->getTraceAsString()
                    )
                );
            } else {
                $logger->error(
                    $ex->getMessage(),
                    array(
                        'request' => $param,
                        'url' => $class,
                        'method' => $name,
                        'error' => $error ? $error->toJson() : $error,
                        'uid' => $code,
                        'exceptionType' => get_class($ex),
                        'trace' => $ex->getTraceAsString()
                    )
                );
            }
            if (!($ex instanceof APIException)) {
                $error = new APIError(
                    APIError::CUSTOM_ERROR,
                    'Unknown error. Please tell core team subCode and we will check it.',
                    $code
                );
            }

        }

        $json = array();
        if($error) {
            $json["error"] = $error ? $error->toJson() : $error;
        } else {
            $json = $data;
        }
        return new JsonResponse($json);
    }

    /**
     * @return string
     */
    protected function getSite()
    {
        return $this->container->get('request_stack')->getMasterRequest()->getSchemeAndHttpHost();
    }

}
