<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */

namespace Nerds\BaseBundle\API;


use Nerds\BaseBundle\Util\APIError;
use Nerds\BaseBundle\Util\APIException;
use Symfony\Component\HttpFoundation\Request;

class BaseAPI {

    const POST = "POST";
    const GET = "GET";

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Controller\Controller
     */
    protected $context;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected function getContainer()
    {
        $container = $this->container;
        /* @var \Symfony\Component\DependencyInjection\ContainerInterface $container */
        return $container;
    }

    /**
     * @param string $url
     * @param array $headers
     * @return mixed
     */
    protected function callExternalUrl($url, $headers = array('Content-type: application/json'))
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (strpos($url, 'https') !== false) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        }

        $response = curl_exec($ch);

        return json_decode($response);
    }

    /**
     * @param $data
     * @param $validatePost
     * @return \Nerds\UserBundle\Entity\User
     * @throws \Exception
     */
    protected function validateAuth($data, $validatePost = true, $optional = false)
    {
        $user = null;
        if ($validatePost){
            $this->validatePost();
        }
        $token = $this->getToken($data);

        if (!$token) {
            if(!$optional) {
                throw new APIException("Empty token", APIError::EMPTY_TOKEN);
            }
        } else {
            $userManager = $this->getUserManager();
            $user = $userManager->findUserByAccessToken($token);
            if (!$user) {
                throw new APIException("Wrong token", APIError::WRONG_TOKEN);
            }
        }
        /* @var \Nerds\UserBundle\Entity\User $user */
        return $user;
    }

    /**
     * @throws \Exception
     */
    protected function validatePost()
    {
        $method = $this->request->getMethod();
        if ($method != self::POST) {
            throw new APIException("Should be post request", APIError::REQUIRE_POST);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\FileBag
     */
    protected function getFiles()
    {
        return $this->request->files;
    }

    /**
     * @param $data
     * @return null|string
     */
    protected function getToken($data)
    {
        $token = $this->request->headers->get('token');

        if ($token) {
            return $token;
        }

        if ($this->isDataExist('token', $data)) {
            return $data['token'];
        }

        return null;
    }

    /**
     * @return \Symfony\Bundle\FrameworkBundle\Controller\Controller
     */
    protected function getContext()
    {
        $context = $this->context;
        /* @var \Symfony\Bundle\FrameworkBundle\Controller\Controller $context */
        return $context;
    }

    /**
     * @param $context
     * @param $container
     * @param $request
     */
    public function setContext($context, $container, $request)
    {
        $this->context = $context;
        $this->container = $container;
        $this->request = $request;
    }

    /**
     * @param $key
     * @param $data
     * @return bool
     */
    protected function isKeyExist($key, $data)
    {
        if (isset($data) && array_key_exists($key, $data)) {
            return true;
        }
        return false;
    }

    /**
     * @param $key
     * @param $data
     * @return bool
     */
    protected function isDataExist($key, $data)
    {
        if (isset($data) && array_key_exists($key, $data) && $data[$key]) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    protected function getSite()
    {
       return $this->request->getSchemeAndHttpHost();
    }

    /**
     * @return \Nerds\UserBundle\Entity\UserManager
     */
    protected function getUserManager()
    {
        return $this->getContainer()->get('fos_user.user_manager');
    }

    /**
     * @return \Nerds\BaseBundle\Entity\BaseRepository
     */
    protected function getFollowingRepository()
    {
        return $this->getContainer()->get('nerds_base.following_repository');
    }

    /**
     * @return \Monolog\Logger;
     */
    protected function getLogger()
    {
        return $this->getContainer()->get('logger');
    }

    /**
     * @param $name
     * @return \Nerds\BaseBundle\Entity\BaseRepository
     */
    protected function getRepository($name)
    {
        $repository = $this->getContext()->getDoctrine()->getRepository($name);
        /* @var \Nerds\BaseBundle\Entity\BaseRepository $repository */
        return $repository;
    }

    protected function getUploader() {
        return $this->getContainer()->get('nerds_base.s3_uploader');
    }

    protected function getVideoUploader() {
        return $this->getContainer()->get('nerds_base.s3_video_uploader');
    }

    protected function getResizer() {
        return $this->getContainer()->get('nerds_base.image_resizer');
    }

    protected function getPushService(){
        return $this->getContainer()->get('nerds_push.push.manager');
    }

    protected function getActivityService(){
        return $this->getContainer()->get('nerds_base.activity');
    }

    protected function getPushActivityService(){
        return $this->getContainer()->get('nerds_push.push.activity');
    }

    protected function setCurl($url, $headers, $post = false, $request = null, $userpwd = false)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, $post);

        if ($post) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        }

        if ($userpwd) {
            curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
        }

        return $ch;
    }

    public function deleteCurl($url, $password = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (!is_null($password)) {
            curl_setopt($ch, CURLOPT_USERPWD, $password);
        }

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    public function curlExec($url, $headers, $post = false, $request = null, $userpwd = false, $json_decode = true)
    {
        $ch = $this->setCurl($url, $headers, $post, $request, $userpwd);

        $result = curl_exec($ch);
        curl_close($ch);

        return $json_decode ? json_decode($result) : $result;
    }

    public function multi_curl($urls, $requests, $headers, $post = false, $userpwd = false)
    {
        $requests = (array) $requests;

        $cmh = curl_multi_init();

        $tasks = array();
        $i = 0;
        foreach ($requests as $request) {

            $tasks[$request] = $this->setCurl($urls[$i], $headers, $post, $request, $userpwd);

            curl_multi_add_handle($cmh, $tasks[$request]);
            $i++;
        }

        $active = null;
        do {
            $mrc = curl_multi_exec($cmh, $active);
        }
        while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && ($mrc == CURLM_OK)) {
                do {
                    $mrc = curl_multi_exec($cmh, $active);
                    $info = curl_multi_info_read($cmh);
                    if ($info['msg'] == CURLMSG_DONE) {
                        $ch = $info['handle'];
                        $request = array_search($ch, $tasks);
                        $tasks[$request] = curl_multi_getcontent($ch);

                        if (curl_errno($ch)) {
                            curl_close($ch);
                        }
                        curl_multi_remove_handle($cmh, $ch);
                        curl_close($ch);
                    }
                }
                while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }

        curl_multi_close($cmh);

        return $tasks;
    }
}
