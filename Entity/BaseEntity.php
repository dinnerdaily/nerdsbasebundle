<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */

namespace Nerds\BaseBundle\Entity;


class BaseEntity  {

    const ACTIVITY_FRIENDSHIP   = 0;
    const ACTIVITY_EVENT        = 1;
    const ACTIVITY_FOLLOW       = 2;
    const ACTIVITY_UNFOLLOW     = 3;
    const ACTIVITY_LIKE         = 4;
    const ACTIVITY_ORDER_STATUS = 5;
    const ACTIVITY_RELEASE      = 6;
    const ACTIVITY_MESSAGE      = 7;
    const ACTIVITY_REVIEW       = 8;
    const ACTIVITY_OFFER        = 3;
    const ACTIVITY_ORDER        = 2;

    const COMPLAIN_NORMAL     = 0;
    const COMPLAIN_SPAM       = 1;
    const COMPLAIN_PROHIBITED = 2;
    const COMPLAIN_BANNED     = 3;

    const ENTRY_TYPE_USER    = 1;
    const ENTRY_TYPE_ITEM    = 2;
    const ENTRY_TYPE_ORDER   = 3;
    const ENTRY_TYPE_OFFER   = 4;
    const ENTRY_TYPE_THREAD  = 5;
    const ENTRY_TYPE_MESSAGE = 6;

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return BaseEntity
     */
    public function setUpdated($updated)
    {
        return $this;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return BaseEntity
     */
    public function setCreated($created)
    {
        return $this;
    }

    public function setCreatedValue()
    {
        $this->setCreated(new \DateTime());
    }


    public function setUpdatedValue()
    {
        $this->setUpdated(new \DateTime());
    }

    public function toJson()
    {
        return array(
            'entity' => 'toJson not implemented'
        );
    }

    public function getActivityUser(){
        return array(
            'entity' => 'getActivityUser not implemented'
        );
    }

    public function getEntityType(){
        return array(
            'entity' => 'getEntityType not implemented'
        );
    }

    public function getEntityId(){
        return array(
            'entity' => 'getEntityId not implemented'
        );
    }

    public function getEntityDate(){
        return array(
            'entity' => 'getEntityId not implemented'
        );
    }

    public function getActivityType(){
        return array(
            'entity' => 'getEntityType not implemented'
        );
    }

    public function getTarget(){
        return array(
            'entity' => 'getTarget not implemented'
        );
    }

    public function getInfoText(){
        return array(
            'entity' => 'getInfoText not implemented'
        );
    }

    public function getPushText(){
        return array(
            'entity' => 'getIsPush not implemented'
        );
    }

    public function getIsRead(){
        return array(
            'entity' => 'getIsRead not implemented'
        );
    }

    public function getIsPush(){
        return array(
            'entity' => 'getIsPush not implemented'
        );
    }

    public function getRelatedEntryType(){
        return null;
    }

    public function getRelatedEntryId(){
        return null;
    }
}
