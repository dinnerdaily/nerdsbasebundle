<?php

namespace Nerds\BaseBundle\Entity;

use Nerds\BaseBundle\Entity\BaseEntity;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Provides common methods for creating entity's media urls
 */
class BaseFileEntity extends BaseEntity
{
    const ORIGINAL_THUMBNAIL = 'originalURL';
    
    const THUMBNAIL_NAME = 'thumbnail.jpg';
    const PICTURE_NAME   = 'image.jpg';
    const VIDEO_NAME     = 'video.mp4';

    const BASE_FOLDER        = '/img/user/';
    
    private $uniqid;

    /**
     * Original thumbnail should remain last, so we don't have to 
     * create additional logic for local image removing after
     * image resize and s3 upload
     */
    protected $thumbnailSizes = array(
        self::ORIGINAL_THUMBNAIL => array('width' => 5000, 'height' => 5000, 'prefix' => ''),
    );

    public function getPictureName($prefix = '')
    {
        return $prefix . $this->getUniqId() . '_' . static::PICTURE_NAME;
    }
    
    public function getThumbnailName($prefix = '')
    {
        return $prefix . $this->getUniqId() . '_' . static::THUMBNAIL_NAME;
    }

    public function getVideoName($prefix = '')
    {
        return $prefix . $this->getUniqId() . '_' . static::VIDEO_NAME;
    }

    public function getRelativePath()
    {
        $folder = static::BASE_FOLDER . substr($this->getUniqId(), 0, 2) . '/';
        return $folder;
    }
    
    public function getPictureRelativePath($prefix = '')
    {
        return $this->getRelativePath().$this->getPictureName($prefix);
    }
    
    public function getThumbnailRelativePath($prefix = '')
    {
        return $this->getRelativePath().$this->getThumbnailName($prefix);
    }
    
    public function getVideoRelativePath($prefix = '')
    {
        return $this->getRelativePath().$this->getVideoName($prefix);
    }
    
    public function getPictureLocalPath($prefix = '')
    {
        return $this->getRootPath().$this->getPictureName($prefix);
    }
    
    public function getThumbnailLocalPath($prefix = '')
    {
        return $this->getRootPath().$this->getThumbnailName($prefix);
    }
    
    public function getVideoLocalPath($prefix = '')
    {
        return $this->getRootPath().$this->getVideoName($prefix);
    }

    public function getRootPath()
    {
        $fs = new Filesystem();
        $dirPath = DOCUMENT_ROOT . $this->getRelativePath();
        if (!$fs->exists($dirPath)) {
            $fs->mkdir($dirPath, 0777);
        }
        return $dirPath;
    }
    
    public function getThumbnailSizes(){
        return $this->thumbnailSizes;
    }
    
    public function setUniqId($uniqid)
    {
        $this->uniqid = $uniqid;
    }
    
    public function getUniqId()
    {
        return $this->uniqid;
    }
}
