<?php

namespace Nerds\BaseBundle\Util;

class APIException extends \Exception
{
    public static function wrongParameterType($message)
    {
        throw new self($message, APIError::WRONG_PARAMETER_TYPE);
    }

    public static function emptyParameterValue($message)
    {
        throw new self($message, APIError::EMPTY_PARAMETER_VALUE);
    }

    public static function accessDenied($message)
    {
        throw new self($message, APIError::ACCESS_DENIED);
    }

    public static function objectNotFound($message)
    {
        throw new self($message, APIError::OBJECT_NOT_FOUND);
    }

    public static function objectAlreadyExists($message)
    {
        throw new self($message, APIError::OBJECT_ALREADY_EXISTS);
    }

    public static function objectNotExists($message)
    {
        throw new self($message, APIError::OBJECT_NOT_EXISTS);
    }
}