<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */

namespace Nerds\BaseBundle\Util;

class JsonConvert {
    public static function toJson($collection, $additionalParam = false) {
        $result = array();
        if(!is_null($collection)) {
            foreach ($collection as $item) {
                /* @var \Nerds\BaseBundle\Entity\BaseEntity $item */
                $result[] = $item->toJson($additionalParam);
            }
        }
        return $result;
    }
}
