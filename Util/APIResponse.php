<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */

namespace Nerds\BaseBundle\Util;



class APIResponse {

    protected $errors;
    protected $data;

    function __construct($data = array(), $errors = null)
    {
        $this->data = $data;
        $this->errors = $errors;
    }

    /**
     * @param mixed $data
     * @return APIResponse
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $errors
     * @return APIResponse
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
