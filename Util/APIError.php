<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */

namespace Nerds\BaseBundle\Util;

/**
 * Class APIError
 * @package Nerds\BaseBundle\Util
 *
 */
class APIError {
    /**
     * @apiGroup APIError: General
     * @apiVersion 1.0.2
     *
     * @apiError  101 Wrong API path
     * @apiError  102 POST request required
     * @apiError  103 Empty token
     * @apiError  104 Wrong token
     * @apiError  105 Access denied
     * @apiError  106 Object not found
     * @apiError  107 Object already exists
     * @apiError  108 Wrong parameter type
     * @apiError  109 Empty parameter value
     * @apiError  110 Object does not exists
     * @apiError  201 Error connecting PayPal services
     * @apiError  601 Postmaster error
     *
     */
    const WRONG_API_PATH        = 101;
    const REQUIRE_POST          = 102;
    const EMPTY_TOKEN           = 103;
    const WRONG_TOKEN           = 104;  //should be used for wrong app token errors only!
    const ACCESS_DENIED         = 105;
    const OBJECT_NOT_FOUND      = 106;
    const OBJECT_ALREADY_EXISTS = 107;
    const WRONG_PARAMETER_TYPE  = 108;
    const EMPTY_PARAMETER_VALUE = 109;
    const OBJECT_NOT_EXISTS     = 110;
    const CUSTOM_ERROR          = 130;
    const PAYPAL_ERROR          = 301;
    const PAYPAL_NOT_VERIFIED   = 302;
    const POSTMASTER_ERROR      = 601;
    const SHIPPO_ERROR          = 602;

    protected $code;
    protected $message;

    protected $subCode;
    protected $subMessage;

    function __construct($code, $message, $subCode = null, $subMessage = null)
    {
        $this->code = $code;
        $this->message = $message;
        $this->subCode = $subCode;
        $this->subMessage = $subMessage;
    }

    /**
     * @param mixed $code
     * @return APIError
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $message
     * @return APIError
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param null $subCode
     * @return APIError
     */
    public function setSubCode($subCode)
    {
        $this->subCode = $subCode;
        return $this;
    }

    /**
     * @return null
     */
    public function getSubCode()
    {
        return $this->subCode;
    }

    /**
     * @param null $subMessage
     * @return APIError
     */
    public function setSubMessage($subMessage)
    {
        $this->subMessage = $subMessage;
        return $this;
    }

    /**
     * @return null
     */
    public function getSubMessage()
    {
        return $this->subMessage;
    }


    public function toJson()
    {
        $result = array(
            'code' => $this->getCode(),
            'message' => $this->getMessage()
        );

        if($this->getSubCode()) {
            $result['subCode'] = $this->getSubCode();
        }

        if($this->getSubMessage()) {
            $result['subMessage'] = $this->getSubMessage();
        }

        return $result;
    }
}
