<?php

namespace Nerds\BaseBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NerdsBaseBundle extends Bundle
{
    public function boot()
    {
        $this->defineDocumentRoot();

        parent::boot();
    }

    private function defineDocumentRoot()
    {
        if (!defined('DOCUMENT_ROOT')) {
            define('DOCUMENT_ROOT', $this->container->get('kernel')->getRootDir() . '/../web');
        }
    }
}
