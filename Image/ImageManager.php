<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */

namespace Nerds\BaseBundle\Image;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;

class ImageManager {
    CONST TYPE_FIT = 'fit';
    CONST TYPE_FILL = 'fill';
    CONST FORMAT_PNG = 'png';
    CONST FORMAT_WEBP = 'webp';
    CONST FORMAT_JPG = 'jpeg';
    CONST FORMAT_GIF = 'gif';

    public static function resize($original, $target, $width, $height, $format = self::FORMAT_PNG, $type = self::TYPE_FIT, $force = false) {
        if(file_exists($original)
        && (!file_exists($target) or $force)) {
            switch($format) {
                case self::FORMAT_PNG:
                    self::resizeGD($original, $target, $width, $height, $type);
                    break;
                case self::FORMAT_WEBP:
                    self::resizeWebp($original, $target, $width, $height, $type);
                    break;
                case self::FORMAT_JPG;
                    self::resizeGD($original, $target, $width, $height, $type);
                    break;
                case self::FORMAT_GIF:
                    self::resizeImagick($original, $target, $width, $height, $type);
                    break;
            }
        }
    }

    private static function resizeWebp($original, $target, $width, $height, $type = self::TYPE_FIT) {
        $file = new \Imagick($original);
        $thumb = null;
        switch($type) {
            case self::TYPE_FIT:
                $newSizes = self::scaleImage(
                    $file->getImageWidth(),
                    $file->getImageHeight(),
                    $width,
                    $height
                );
                $thumb = $file->cropThumbnailImage(
                    new Box($newSizes->getWidth(), $newSizes->getHeight())
                );
                break;
            case self::TYPE_FILL:
                $newSizes = new ImageSize($width, $height);
                if ($file->getImageWidth() > $file->getImageHeight()) {
                    $width  = $file->getImageWidth() * ($newSizes->getHeight() / $file->getImageHeight());
                    $height = $newSizes->getHeight();
                    $cropPoint = new Point((max($width - $newSizes->getWidth(), 0)) / 2, 0);
                } else {
                    $width  = $newSizes->getWidth();
                    $height = $file->getImageHeight()*($newSizes->getWidth() / $file->getImageWidth());
                    $cropPoint = new Point(0, (max($height-$newSizes->getHeight(), 0)) / 2);
                }

                $box = new Box($width, $height);
                //we scale the image to make the smaller dimension fit our resize box
                $thumb = $file->cropThumbnailImage($box, ImageInterface::THUMBNAIL_OUTBOUND);

                //and crop exactly to the box
                $thumb = $thumb->crop($cropPoint,  new Box($newSizes->getWidth(), $newSizes->getHeight()));
                break;
        }
        $file->stripImage();
        $file->setImageFormat('webp');
        $file->setImageAlphaChannel(\Imagick::ALPHACHANNEL_ACTIVATE);
        $file->setBackgroundColor(new \ImagickPixel('transparent'));
        $file->writeImage($target);
        $thumb->save($target, array('flatten' => false));
    }

    private static function resizeGD($original, $target, $width, $height, $type = self::TYPE_FIT) {
        $imagine = new \Imagine\Gd\Imagine();
        $file = $imagine->open($original);
        $sizes = $file->getSize();
        $thumb = null;
        switch($type) {
            case self::TYPE_FIT:
                $newSizes = self::scaleImage(
                    $sizes->getWidth(),
                    $sizes->getHeight(),
                    $width,
                    $height
                );
                $thumb = $file->thumbnail(
                    new Box($newSizes->getWidth(), $newSizes->getHeight())
                );
                break;
            case self::TYPE_FILL:
                $newSizes = new ImageSize($width, $height);
                if ($sizes->getWidth() > $sizes->getHeight()) {
                    $width  = $sizes->getWidth() * ($newSizes->getHeight() / $sizes->getHeight());
                    $height = $newSizes->getHeight();
                    $cropPoint = new Point((max($width - $newSizes->getWidth(), 0)) / 2, 0);
                } else {
                    $width  = $newSizes->getWidth();
                    $height = $sizes->getHeight()*($newSizes->getWidth() / $sizes->getWidth());
                    $cropPoint = new Point(0, (max($height-$newSizes->getHeight(), 0)) / 2);
                }

                $box = new Box($width, $height);
                //we scale the image to make the smaller dimension fit our resize box
                $thumb = $file->thumbnail($box, ImageInterface::THUMBNAIL_OUTBOUND);

                //and crop exactly to the box
                $thumb = $thumb->crop($cropPoint,  new Box($newSizes->getWidth(), $newSizes->getHeight()));
                break;
        }
        $thumb->save($target, array('flatten' => false));
    }

    private static function resizeImagick($original, $target, $width, $height, $type = self::TYPE_FIT) {
        $animation = new \Imagick($original);
        $images = $animation->coalesceImages();
        /* @var \Imagick $images */

        $animation->setResourceLimit(\Imagick::RESOURCETYPE_MEMORY, 120);
        $animation->setResourceLimit(\Imagick::RESOURCETYPE_MAP, 120);

        list($sizeWidth, $sizeHeight, $tt, $attr) = getimagesize($original);
        switch($type) {
            case self::TYPE_FIT:
                $newSizes = self::scaleImage(
                    $sizeWidth,
                    $sizeHeight,
                    $width,
                    $height
                );
                foreach ($images as $frame) {
                    /* @var \Imagick $frame */
                    $frame->thumbnailImage($newSizes->getWidth(), $newSizes->getHeight(), true);
                    $frame->setImagePage($newSizes->getWidth(), $newSizes->getHeight(), 0, 0);
                }
                break;
            case self::TYPE_FILL:
                $newSizes = new ImageSize($width, $height);
                foreach ($images as $frame) {
                    /* @var \Imagick $frame */
                    $frame->cropThumbnailImage($newSizes->getWidth(), $newSizes->getHeight());
                    $frame->setImagePage($newSizes->getWidth(), $newSizes->getHeight(), 0, 0);
                }
                break;
        }

        $animation = $images->deconstructImages();
        /* @var \Imagick $animation */
        $animation->writeImages($target, true);
    }

    /**
     * Calculate new image dimensions to new constraints
     *
     * @param string $width size in pixels
     * @param string $height size in pixels
     * @param string $targetWidth size in pixels
     * @param string $targetHeight size in pixels
     * @return ImageSize
     */
    public static function scaleImage($width, $height, $targetWidth, $targetHeight)
    {
        list($newWidth, $newHeight) = array($width, $height);

        if ($width >= $targetWidth || $height >= $targetWidth) {
            $ratioWidth = 0;
            $ratioHeight = 0;
            if ($width > 0) {
                $ratioWidth = $targetWidth / $width;
            }
            if ($height > 0) {
                $ratioHeight = $targetHeight / $height;
            }

            if ($ratioWidth > $ratioHeight) {
                $ratio = $ratioHeight;
            } else {
                $ratio = $ratioWidth;
            }

            $newWidth = intval($width * $ratio);
            $newHeight = intval($height * $ratio);
        }

        return new ImageSize($newWidth, $newHeight);
    }
}
