<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */

namespace Nerds\BaseBundle\Image;


class ImageSize {
    private $width;
    private $height;

    function __construct($width = 0, $height = 0)
    {
        $this->height = $height;
        $this->width = $width;
    }

    /**
     * @param mixed $height
     * @return ImageSize
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $width
     * @return ImageSize
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }
}
