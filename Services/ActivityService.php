<?php
/**
 * Created by PhpStorm.
 * User: dmitriev
 * Date: 19.12.2014
 * Time: 10:58
 */

namespace Nerds\BaseBundle\Services;

use Nerds\UserBundle\Entity\Activity;
use Nerds\UserBundle\Entity\ActivityRepository;
use Nerds\PushBundle\Services\PushService;

class ActivityService {

    protected $activitiesToPush = array();
    /** @var \Nerds\PushBundle\Services\PushService $push */
    protected $push;
    /** @var \Nerds\UserBundle\Entity\ActivityRepository $activityRepository */
    protected $activityRepository;

    public function __construct(PushService $push = null, ActivityRepository $activityRepository, $entityManager)
    {
        $this->push = $push;
        $this->activityRepository = $activityRepository;
        $this->entityManager = $entityManager;
    }
    
    public function restoreFromQueue($params) {
        $entity = $this->entityManager
                    ->getRepository($params['object']['class'])
                    ->find($params['object']['id']);
        $this->$params['method']($entity);
    }

    /**
     * @param \Nerds\BaseBundle\Entity\BaseEntity $object
     * @param bool $postponePush
     * @param bool $category
     */
    public function createActivity($object, $postponePush = FALSE, $category = null){
        if (is_array($object->getTarget())) {
            $targetDevices = array();
            foreach($object->getTarget() as $user) {
                /** @var \Nerds\UserBundle\Entity\User $user */
                foreach ($user->getDevices() as $device){
                    $targetDevices[] = $device;
                }
                $this->buildActivityObject($object, $user, FALSE, null, $category);
            }
            if ($object->getIsPush()){
                $this->pushActivitiesForDevices($object, $targetDevices, $category);
            }
        }
        else {
            $this->buildActivityObject($object, $object->getTarget(), TRUE, null, $category);
        }

        if (!$postponePush){
            $this->pushActivities();
        }
    }

    /**
     * @param array $entitiesList
     */
    public function createActivitiesFromList($entitiesList){
        foreach ($entitiesList as $entity){
            $this->createActivity($entity, TRUE);
        }

        $this->pushActivities();
    }

    /**
     * @param \Nerds\UserBundle\Entity\User $user
     * @param \Nerds\UserBundle\Entity\User $sender
     * @param string $activityType
     * @param string $pushText
     * @param string $alertText
     * @param string $category
     * @param \Nerds\UserBundle\Entity\Activity $entryObj
     */
    public function createDirectActivity(
        $user,
        $sender,
        $activityType,
        $pushText,
        $entryObj,
        $alertText = null,
        $category = null
    )
    {
        $activity = new Activity();
        $activity->setUser($sender);
        $activity->setActivityType($activityType);
        $activity->setEntityType($entryObj->getEntityType());
        $activity->setEntityId($entryObj->getEntityId());
        $activity->setTarget($user);
        $activity->setText(is_null($alertText) ? $pushText : $alertText);
        $activity->setPushText($pushText);
        $activity->setEntityDate($entryObj->getEntityDate());
        $activity->setIsRead($entryObj->getIsRead());
        $activity->setCategory($category);
        $activity->setRelatedEntryId($entryObj->getRelatedEntryId());
        $activity->setRelatedEntryType($entryObj->getRelatedEntryType());

        $this->activityRepository->save($activity);

        $custom = array(
            'entityType'=> $activity->getEntityType(),
            'entityId'  => $entryObj->getEntityId()
        );

        if ($activity->getRelatedEntryId()){
            $custom['id'] = $activity->getRelatedEntryId();
            $custom['entityClass'] = $activity->getRelatedEntryType();
        }

        $this->push->add(
            $activity->getTarget()->getDevices(),
            $pushText,
            array(
                'custom' => $custom,
                'category'  => (string)$category
            ),
            null,
            $this->activityRepository->countUnreadActivities($activity->getTarget()->getId())
        );
    }

    private function pushActivities(){
        if ($this->push != null) {
            foreach ($this->activitiesToPush as $activity) {
                /** @var \Nerds\UserBundle\Entity\Activity $activity */
                $devices = $activity->getTarget()->getDevices();
                /** @var \Nerds\UserBundle\Entity\ActivityRepository $activityRepository */

                $custom = array(
                    'entityType'=> $activity->getEntityType(),
                    'entityId'  => $activity->getEntityId()
                );

                if ($activity->getRelatedEntryId()){
                    $custom['id'] = $activity->getRelatedEntryId();
                    $custom['entityClass'] = $activity->getRelatedEntryType();
                }

                $this->push->add(
                    $devices,
                    $activity->getPushText(),
                    array(
                        'custom' => $custom,
                        'category'  => (string)$activity->getCategory()
                    ),
                    null,
                    $this->activityRepository->countUnreadActivities($activity->getTarget()->getId())
                );
            }
            $this->activitiesToPush = array();
        }
    }

    /**
     * @param \Nerds\BaseBundle\Entity\BaseEntity $entity
     * @param array $devices
     * @param int $category
     */
    protected function pushActivitiesForDevices($entity, $devices, $category = null){
        if ($this->push != null){
            $custom = array(
                'entityType'=> $entity->getEntityType(),
                'entityId'  => $entity->getEntityId()
            );

            if ($entity->getRelatedEntryId()){
                $custom['id'] = $entity->getRelatedEntryId();
                $custom['entityClass'] = $entity->getRelatedEntryType();
            }

            $this->push->add(
                $devices,
                $entity->getPushText(),
                array(
                    'custom' => $custom,
                    'category'  => (string)$category
                ),
                null,
                1
            );
        }
    }

    /**
     * @param \Nerds\BaseBundle\Entity\BaseEntity $entryObj
     * @param \Nerds\UserBundle\Entity\User $user
     * @param $custom_text
     * @param bool $pushIt
     * @param int $category
     */
    protected function buildActivityObject($entryObj, $user, $pushIt = TRUE, $custom_text = null, $category = null)
    {
        $activity = new Activity();
        $activity->setUser($entryObj->getActivityUser());
        $activity->setActivityType($entryObj->getActivityType());
        $activity->setEntityType($entryObj->getEntityType());
        $activity->setEntityId($entryObj->getEntityId());
        $activity->setTarget($user);
        $activity->setText($entryObj->getInfoText());
        $activity->setCategory($category);
        $activity->setRelatedEntryId($entryObj->getRelatedEntryId());
        $activity->setRelatedEntryType($entryObj->getRelatedEntryType());

        if (!is_null($custom_text)) {
            $activity->setPushText($custom_text);
        } else {
            $activity->setPushText($entryObj->getPushText());
        }

        $activity->setEntityDate($entryObj->getEntityDate());
        $activity->setIsRead($entryObj->getIsRead());

        $this->activityRepository->save($activity);

        if($entryObj->getIsPush() && $pushIt){
            $this->activitiesToPush[] = $activity;
        }
    }

    /**
     * @param \Nerds\UserBundle\Entity\User ArrayCollection $users
     * @param \Nerds\UserBundle\Entity\User $sender
     * @param string $activityType
     * @param string $pushText
     * @param string $alertText
     * @param int $category
     * @param \Nerds\UserBundle\Entity\Activity $entryObj
     */
    public function createDirectActivities(
        $users,
        $sender,
        $activityType,
        $pushText,
        $entryObj,
        $alertText = null,
        $category = null
    )
    {
        if (is_array($users) && count($users) > 0) {
            foreach ($users as $user) {
                $activity = new Activity();
                $activity->setUser($sender);
                $activity->setActivityType($activityType);
                $activity->setEntityType($entryObj->getEntityType());
                $activity->setEntityId($entryObj->getEntityId());
                $activity->setTarget($user);
                $activity->setText(is_null($alertText) ? $pushText : $alertText);
                $activity->setPushText($pushText);
                $activity->setEntityDate($entryObj->getEntityDate());
                $activity->setIsRead($entryObj->getIsRead());
                $activity->setCategory($category);
                $activity->setRelatedEntryId($entryObj->getRelatedEntryId());
                $activity->setRelatedEntryType($entryObj->getRelatedEntryType());

                $this->activityRepository->save($activity);

                $custom = array(
                    'entityType'=> $activity->getEntityType(),
                    'entityId'  => $activity->getEntityId()
                );

                if ($activity->getRelatedEntryId()){
                    $custom['id'] = $activity->getRelatedEntryId();
                    $custom['entityClass'] = $activity->getRelatedEntryType();
                }

                $this->push->add(
                    $activity->getTarget()->getDevices(),
                    $pushText,
                    array(
                        'custom' => $custom,
                        'category'  => (string)$category
                    ),
                    null,
                    $this->activityRepository->countUnreadActivities($activity->getTarget()->getId())
                );
            }

            $this->pushActivities();
        }
    }
}