<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */

namespace Nerds\BaseBundle\Services;

use Aws\Sts\StsClient;

class S3VideoUploader {
    private $service;

    public function __construct($key, $secret, $bucket, $v4 = null, $region = null)
    {
        $this->bucket = $bucket;

        if ($v4)
        {
            $this->service = StsClient::factory(array('signature' => 'v4', 'region' => $region,'key' => $key, 'secret' => $secret ));
        }
        else
        {
            $this->service = StsClient::factory(array('key' => $key, 'secret' => $secret ));
        }
    }

    public function getData($name, $url)
    {
        $result = $this->service->getFederationToken(
            array(
                'Name'            => $name,
                'DurationSeconds' => 3600,
                'Policy'          => json_encode(array(
                    'Statement' => array(
                        array(
                            'Sid'      => uniqid(),
                            'Action'   => array('s3:PutObject', 's3:PutObjectAcl'),
                            'Effect'   => 'Allow',
                            'Resource' => $url
                        )
                    )
                ))
            )
        );

        return $result->get('Credentials');
    }
}
