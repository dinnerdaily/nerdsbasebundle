<?php
/**
 * Created by PhpStorm.
 * User: dmitriev
 * Date: 04.12.2014
 * Time: 12:00
 */

namespace Nerds\BaseBundle\Services;

use Nerds\BaseBundle\Image\ImageManager;

class ResizeService {
    /**
     * @var \Nerds\BaseBundle\Image\ImageManager $resizer
     */
    protected $resizer;

    public function __construct(){
        $this->resizer = new ImageManager();
    }

    /**
     * @param string $original
     * @param string $target
     * @param int $width
     * @param int $height
     * @param string $format
     * @param string $type
     * @param bool $force
     */
    public function resize($original, $target, $width, $height, $format = "png", $type = "fit", $force = false){

        $this->resizer->resize($original, $target, $width, $height, $format, $type, $force);
    }
}