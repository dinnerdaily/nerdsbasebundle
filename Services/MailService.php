<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */
namespace Nerds\BaseBundle\Services;

class MailService {
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    protected $router;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
     */
    protected $templating;

    /**
     * @var array
     */
    protected $parameters;

    public function __construct($mailer, $router, $templating, array $parameters)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->parameters = $parameters;
    }

    /**
     * @param string $toEmail
     * @param string $template
     * @param array $params
     */
    public function sendTemplateEmail($toEmail, $template, $params) {
        if(array_key_exists('url', $params)) {
            $url = $this->router->generate($params['url']['name'], $params['url']['data'], true);
            $params['url'] = $url;
        }
        $rendered = $this->templating->render($template, $params);
        $this->sendRenderedEmail($toEmail, $rendered);
    }

    /**
     * @param string $toEmail
     * @param string $renderedTemplate
     */
    public function sendRenderedEmail($toEmail, $renderedTemplate) {
        $renderedLines = explode("\n", trim($renderedTemplate));
        $subject = $renderedLines[0];
        $body = implode("\n", array_slice($renderedLines, 1));

        $this->sendMail($toEmail, $subject, $body);
    }

    /**
     * @param string $toEmail
     * @param string $subject
     * @param string $body
     */
    public function sendMail($toEmail, $subject, $body)
    {
        $fromAddress = $this->parameters['from_email'];
        $fromName = $this->parameters['from_name'];

        /* @var \Swift_Message $message */
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromAddress, $fromName)
            ->setTo($toEmail)
            ->setContentType("text/html")
            ->setBody($body);

        $this->mailer->send($message);
    }
}
