<?php
/**
 * @author Sergey Slepokurov sergey@slepokurov.com
 */

namespace Nerds\BaseBundle\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Aws\S3\S3Client;
use Aws\Common\Aws;
use Aws\S3\Exception\S3Exception;
class S3Uploader {
    private $service;

    public function __construct($key, $secret, $bucket, $baseS3Url, $localBase, $v4 = null, $region = null)
    {
        $this->bucket = $bucket;
        $this->baseS3Url = $baseS3Url;
        $this->localBase = $localBase;

        if ($v4)
        {
            $this->service = S3Client::factory(array('signature' => 'v4', 'region' => $region,'key' => $key, 'secret' => $secret ));
        }
        else
        {
            $this->service = S3Client::factory(array('key' => $key, 'secret' => $secret ));
        }
    }

    /**
     * @param string $name
     * @param string|UploadedFile $file
     * @return string|bool
     */
    public function upload($name, $file)
    {
        $filename = '';
        switch(gettype($file)){
            case 'string':
                $filename = $file;
                break;
            case 'object':
                switch(get_class($file)){
                    case 'Symfony\Component\HttpFoundation\File\UploadedFile':
                        /* @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $filename = $file->getPathname();
                        break;
                }
                break;
            default:
                $filename = uniqid();
                break;
        }

        try {
            $this->service->upload($this->bucket, $name, fopen($filename, 'r'), 'public-read');
        } catch (S3Exception $e) {
            return "There was an error uploading the file.";
        }
        return true;
    }
    
    /**
     * Generates S3 url without uploading file
     * @param string $name
     * @return string|bool
     */
    public function getPreuploadUrl($name)
    {
        return $this->baseS3Url.$name;
    }
    
    /**
     * Generates local url to use untill 
     * RabbitMQ queue processed
     * @param string $name
     * @return string|bool
     */
    public function getLocalUrl($name)
    {
        return 'http://' . $this->localBase . $name;
    }
    
    /**
     * UNLINK function alias to use with RabbitMQ queue
     * @param string $name
     * @return void
     */
    public function deleteOriginal($name)
    {
        unlink($name);
    }
    
    public function url($name)
    {
        return $this->service->getObjectUrl($this->bucket, $name);
    }
}
